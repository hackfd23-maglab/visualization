# Traffic Insights
_Hackathon Fulda 2023_

Traffic Insights is a simple, reactive frontend, based on the modern, open-source [Svelte](https://svelte.dev/) JS framework.

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

![Screenshot](screenshot.png)

## Setup

Copy `.env.example` to `.env` and customize productive or development api server urls.

The default tile server from [CARTO](https://carto.com) is free to use for non-business use cases.

To test and use this frontend, a working instance of the [api server](https://gitlab.com/hackfd23-maglab/api) is required.

### Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

Everything inside `src/lib` is part of your library, everything inside `src/routes` can be used as a showcase or preview app.

### Building

To create a production version of this app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

## License
This project is MIT licensed.
