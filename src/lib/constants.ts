export const variables = {
    API_URI: import.meta.env.DEV
        ? import.meta.env.VITE_API_URI_DEV
        : import.meta.env.VITE_API_URI_PROD,
    TILE_SERVER_URL: import.meta.env.VITE_TILE_SERVER_URL ?? 'https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png'
};

export const apiRoutes = {
    linePoints: '/getPaths/{startEpoch}/{endEpoch}',
    allLinePoints: '/getPaths',
    timeLimits: '/timelimits',
    beacons: '/getBeacons'
}

export const defaultSliderRange = 300000;
export const defaultViewLatitude = 50.56238011561546;
export const defaultViewLongitude = 9.682667083892214;
