export interface TimeLimits {
	earliest: number,
	latest: number
}
