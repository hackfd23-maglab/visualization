export interface Beacon {
	latitude: number,
	longitude: number
}
