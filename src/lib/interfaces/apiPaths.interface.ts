export interface APIPaths {
	id: string,
	startTime: number,
	endTime: number,
	path: number[][]
}
