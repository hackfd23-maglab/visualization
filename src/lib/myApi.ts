import { apiRoutes, variables } from "./constants";
import type { APIPaths } from "./interfaces/apiPaths.interface";
import type { TimeLimits } from "./interfaces/timelimits.interface";
import type { Beacon } from "./interfaces/beacon.interface";

export const getLinePointsByEpoch = async (startEpoch: string = "1", endEpoch: string = "1"): Promise<APIPaths[]> => {
    const response = await getJson<APIPaths[]>(
        apiRoutes.linePoints
            .replace('{startEpoch}', startEpoch)
            .replace('{endEpoch}', endEpoch)
    );
    if (response !== undefined) {
        return response;
    }
    return undefined;
};

export const getAllLinePoints = async (): Promise<APIPaths[]> => {
    return await getJson<APIPaths[]>(apiRoutes.allLinePoints) ?? undefined;
};

export const getTimeLimits = async (): Promise<TimeLimits> => {
    return await getJson<TimeLimits>(apiRoutes.timeLimits) ?? undefined;
};

export const getBeacons = async (): Promise<Beacon[]> => {
    return await getJson<Beacon[]>(apiRoutes.beacons) ?? undefined;
};

export const getJson = async <T>(url: string): Promise<T | undefined> => {
    const res = await fetch(variables.API_URI + url, {
        headers: {
            Accept: 'application/json'
        }
    });
    if (res.status >= 400) {
        console.error(res);
    }
    return (await res.json()) as T;
};
