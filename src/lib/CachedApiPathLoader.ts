import type { APIPaths } from "./interfaces/apiPaths.interface";
import { getAllLinePoints, getLinePointsByEpoch } from "./myApi";

export class CachedApiPathLoader {

    /**
     * Use this factory method if you want to use the "loadAllAtOnce" feature.
     *
     * @param loadAllAtOnce
     */
    public static newInstance = async (loadAllAtOnce: boolean = false) => {
        const loader = new CachedApiPathLoader();
        if (loadAllAtOnce) {
            loader.allPathsCache = await getAllLinePoints();
            loader.loadAllAtOnce = true;
        }
        return loader;
    }

    private loadAllAtOnce = false;
    private allPathsCache: APIPaths[] = [];

    /**
     * Timestamp ranges that were already fetched from the server.
     * Avoid using startTime/endTime from the paths, that does not indicate the completeness of this cache solution.
     *
     * [
     *  [startEpoch1, endEpoch1],
     *  [startEpoch2, endEpoch2],
     *  ...
     * ]
     * @private
     */
    private cachedTsRanges: number[][] = [];

    private getMissingTSRange = (startEpoch: number, endEpoch: number): number[] | null => {
        let requiredRange = [startEpoch, endEpoch];

        // check exact matches or matches between already cached request timestamps
        for (const tsRange of this.cachedTsRanges) {
            if (tsRange.at(0) <= startEpoch && tsRange.at(1) >= endEpoch) {
                return null;
            }
        }

        // todo: we could check edge cases or just ignore them for now

        return requiredRange;
    }

    public getPathsForTsRange = async (requestedStartEpoch: number, requestedEndEpoch: number): Promise<APIPaths[]> => {
        // todo: cache merges disabled due to buggy api server responses
        /*const missingTSRange = this.getMissingTSRange(requestedStartEpoch, requestedEndEpoch);
        if (missingTSRange !== null) {
            const newPaths = await getAllLinePoints("" + missingTSRange.at(0), "" + missingTSRange.at(1));
            this.allPathsCache.push(...newPaths);
            this.cachedTsRanges.push([missingTSRange.at(0), missingTSRange.at(1)]);
        }*/

        if (!this.loadAllAtOnce) {
            console.log(this.loadAllAtOnce);
            this.allPathsCache = await getLinePointsByEpoch("" + requestedStartEpoch, "" + requestedEndEpoch);
        }

        let collection: APIPaths[] = [];
        for (const paths of this.allPathsCache) {
            // load paths, with longer time range that my selected time range
            // path time range starts in the past and remains at least within our selected range
            if (requestedStartEpoch >= paths.startTime && requestedStartEpoch <= paths.endTime) {
                collection.push(paths);
            } else if (paths.startTime >= requestedStartEpoch && paths.endTime <= requestedEndEpoch) {
                // load paths, with shorter time range than the slider range difference
                collection.push(paths);
            }
        }
        return collection;
    }
}
