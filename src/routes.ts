/* eslint-disable */

// Components: only Home, Loading and NotFound are statically included in the bundle
import { SvelteComponent } from 'svelte';
import type { RouteDetail, WrappedComponent } from 'svelte-spa-router';
import Home from "./routes/Home.svelte";

// Export the route definition object
export default {
	'*': Home as typeof SvelteComponent
};
