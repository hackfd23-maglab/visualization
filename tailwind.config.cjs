/** @type {import('tailwindcss').Config}*/
const config = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			colors: {
				primary: '#2424ff',
				'primary-element': '#1b46ff',
				'primary-element-hover': '#365dff',
				'primary-element-light': '#e5eff6',
				'primary-light': '#fff',
				'primary-light-text': 'rgb(17 24 39)',
				'primary-light-button-text': 'rgb(17 24 39)',
				'primary-dark': '#1f2937',
				'primary-dark-card': '#24323e',
				'primary-dark-text': 'rgb(229 231 235)',
				'primary-element-dark-hover': '#30435e',
				'primary-trow-dark-hover': '#283850',
				'primary-dark-button': '#30435e',
				'primary-dark-button-text': 'rgb(229 231 235)',
				'primary-dark-button-hover': '#374c6b'
			},
			maxWidth: {
				'1/2': '50%',
				15: '15rem'
			}
		}
	},
	darkMode: 'class',
	plugins: []
};

module.exports = config;
